let {makeDirectory,toGenerateRandomFiles,toDeleteGenerateRandomFiles}=require('../problem1.cjs');

let x = Math.floor((Math.random() * 10) + 1);

async function tocreateAndDelete(){
    let makeDirectories=await makeDirectory();
   
    let generateFiles=await toGenerateRandomFiles(x);
    
    let deleteFiles=await toDeleteGenerateRandomFiles(x);
    
    console.log("files Created And Deleted SuccessFully")
}

tocreateAndDelete();