let { readingFile, writingFile, deleteFiles, appendFile } = require('../problem2.cjs');

async function manipulatingFiles() {

    try {
        // Problem 1
        let lipsumFilePath = "/home/ronin/Desktop/drills-2/fsAsyncAwait/lipsum.txt";
        let lipsumFileData = await readingFile(lipsumFilePath);

        // Problem 2
        let dataToUpper = lipsumFileData.toUpperCase();
        let upperCaseFilePath = "/home/ronin/Desktop/drills-2/fsAsyncAwait/upperCaseFile.txt";
        await writingFile(upperCaseFilePath, dataToUpper);

        // problem 2--append
        let filestoSavePath = "/home/ronin/Desktop/drills-2/fsAsyncAwait/filenames.txt"
        await writingFile(filestoSavePath, upperCaseFilePath);

        // Problem 3
        let upperCaseData = await readingFile(upperCaseFilePath);
        let dataToLower = upperCaseData.toLowerCase().split(".").join("\n");
        let lowerCaseFilePath = "/home/ronin/Desktop/drills-2/fsAsyncAwait/lowerCaseFile.txt";
        await writingFile(lowerCaseFilePath, dataToLower);

        // problem 3--append
        await writingFile(filestoSavePath, lowerCaseFilePath);

        // Problem 4
        let lowerCaseData = await readingFile(lowerCaseFilePath);
        let sortedData = (lowerCaseData + upperCaseData).split(' ').sort().join('\n');
        let sortedFilePath = "/home/ronin/Desktop/drills-2/fsAsyncAwait/sortedFile.txt";
        await writingFile(sortedFilePath, sortedData);

        // problem 4--append
        await writingFile(filestoSavePath, sortedFilePath);

        // Problem 5
        let filesPaths = await readingFile(filestoSavePath);
        const unlinkPromises = filesPaths.split('\n').map(async (fileName) => {
            await deleteFiles(fileName);
            console.log(`Deleted ${fileName}`);
        });
        await Promise.all(unlinkPromises);
    }catch(error){
        // console.log(error);
    }

}

manipulatingFiles();