let fs=require("fs");
let path=require('path')

function makeDirectory(){
    return new Promise((resolve,reject)=>{
        let pathtoCreateDirectory=path.join(__dirname,"tests");
        fs.mkdir(pathtoCreateDirectory,{recursive:true},(err)=>{
            if(err){
                reject(err);
            }else{
                resolve("successly Created directory");
            }
        })
    })
}


function toGenerateRandomFiles(x){
    return new Promise((resolve,reject)=>{
        let pathtoCreateDirectory=path.join(__dirname,"tests");
        for(let i=0;i<x;i++){
            fs.writeFile(pathtoCreateDirectory+`/file${i}.JSON`,"dummydata",(err)=>{
                if(err){
                    reject(err);
                }else{
                    resolve("files Created Successfully")
                }
            })
        }
    })
}


function toDeleteGenerateRandomFiles(x){
    return new Promise((resolve,reject)=>{
        let pathtoCreateDirectory=path.join(__dirname,"tests");
        for(let i=0;i<x;i++){
            fs.unlink(pathtoCreateDirectory+`/file${i}.JSON`,(err)=>{
                if(err){
                    reject(err);
                }else{
                    resolve("files deleted successfully")
                }
            })
        }
    })
}


module.exports={makeDirectory,toGenerateRandomFiles,toDeleteGenerateRandomFiles}