let fs = require('fs');
let path=require('path');

// Reading Files
function readingFile(paths) {
    return new Promise((resolve, reject) => {
        fs.readFile(paths, 'utf8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    })
}

// Writing Files
function writingFile(paths, dataToFile) {
    return new Promise((resolve, reject) => {
        fs.writeFile(paths, dataToFile + '\n', { flag: 'a' }, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        })
    })
}

// to deleteFiles
function deleteFiles(item) {
    return new Promise((resolve, reject) => {
        fs.unlink(item, (error) => {
            if (error) {
                reject("Error happend while deleting the file");
            }else{
                resolve();
            }
        })
    })

}


// append file
function appendFile(paths, dataToFile) {
    return new Promise((resolve, reject) => {
      fs.appendFile(paths, dataToFile, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
  


module.exports={readingFile,writingFile,deleteFiles,appendFile}
